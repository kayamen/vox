'use strict';

let gulp       = require('gulp');
let imagemin   = require('gulp-imagemin');
let sass       = require('gulp-sass');
let concatCss  = require('gulp-concat-css');
let rename     = require('gulp-rename');
let minifyCSS  = require('gulp-minify-css');
let clean      = require('gulp-clean');
let watch      = require('gulp-watch');

gulp.task('images', () => {
    gulp
        .src('src/**/*.{gif,jpg,png,svg,pdf,ico}')
        .pipe(imagemin({
            optimizationLevel: 4,
            progressive: true,
            interlaced: true
        }))
        .pipe(gulp.dest('app'))
    ;
});

gulp.task('htmls', () => {
    gulp
        .src('src/**/*.html')
        .pipe(gulp.dest('app'))
    ;
});

gulp.task('styles', () => {
    gulp
        .src('src/style.scss')
        .pipe(sass())
        .pipe(concatCss('style.css'))
        .pipe(rename({suffix: '.min'}))
        .pipe(minifyCSS())
        .pipe(gulp.dest('app'))
    ;
});

gulp.task('clean', () => {
    gulp
        .src(['app/**/*.{html,css,jpg,gif,png}', 'app/bundle.js'], {read: false})
        .pipe(clean())
    ;
});

gulp.task('watch', () => {
    gulp.watch('src/**/*.{gif,jpg,png,svg,pdf,ico}', ['images']);
    gulp.watch(['index.html', 'src/**/*.html'], ['htmls']);
    gulp.watch('src/**/*.scss', ['styles']);
});

gulp.task('default', ['clean', 'images', 'htmls', 'styles']);
