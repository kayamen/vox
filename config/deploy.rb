lock '>= 3.4.0'

set :application, 'angular2-skeleton'
set :repo_url, "https://github.com/dudemelo/#{fetch(:application)}.git"
set :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }.call
set :deploy_to, "/var/www/#{fetch(:application)}"

after 'deploy:published', 'symbolic:create'
