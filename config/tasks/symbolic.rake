namespace :symbolic do
    task :create do
        on roles(:web) do
            execute "if [ ! -h '/var/www/#{fetch(:application)}' ]; then ln -sf #{current_path} /var/www/#{fetch(:application)}; fi"
        end
    end
end
