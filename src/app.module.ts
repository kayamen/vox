    import { NgModule }      from '@angular/core';
    import { BrowserModule } from '@angular/platform-browser';
    import { FormsModule }   from '@angular/forms';

    import { AppComponent }   from './app.component';
    import { PessoaComponent } from './pessoa/pessoa.component';
    import {routing, appRoutingProviders} from "./app.routing";
    import { PessoaModule } from './pessoa/pessoa.module';



    @NgModule({
        imports: [BrowserModule, FormsModule, routing, PessoaModule],
        declarations: [ AppComponent, PessoaComponent ],
        bootstrap:    [ AppComponent ],
        providers: [appRoutingProviders]

    })
    export class AppModule { }
