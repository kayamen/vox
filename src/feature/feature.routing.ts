import { ModuleWithProviders }             from '@angular/core';
import { Routes, RouterModule }            from '@angular/router';
import { FeatureComponent }                from './feature.component';

const featureRoutes: Routes = [{
    path: '',
    component: FeatureComponent,
    children: [
        // { path: '/another-feature', component: AnotherFeatureComponent },
    ]
}];

export const FeatureRouting: ModuleWithProviders = RouterModule.forChild(featureRoutes);
