import { NgModule }         from '@angular/core';
import { BrowserModule }    from '@angular/platform-browser';
import { FeatureRouting }   from './feature.routing';
import { FeatureComponent } from './feature.component';

@NgModule({
    imports:      [BrowserModule, FeatureRouting],
    declarations: [FeatureComponent],
    exports:      [FeatureComponent],
})
export class FeatureModule {

}
