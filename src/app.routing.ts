import { ModuleWithProviders }   from '@angular/core';
import { Routes, RouterModule }  from '@angular/router';
import { PessoaComponent }      from './pessoa/pessoa.component';


const appRoutes:Routes = [
    { path: '**', component: PessoaComponent }
];

export const appRoutingProviders:Array<any> = [];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
