import { Component } from '@angular/core';
import { ListagemService } from "./listagem.service";
import { Pessoa } from "../../model/pessoa";

@Component({
    selector: 'pessoa-listagem',
    templateUrl: 'app/pessoa/listagem/listagem.html',
    providers: [ListagemService]
})
export class ListagemComponent {
    private listagemService: ListagemService;
    private pessoa: Pessoa;

    public constructor(listagemService: ListagemService) {
        this.listagemService = listagemService;
        this.pessoa = new Pessoa();
    }

    public getPessoa(): Array<Pessoa> {
        return this.listagemService.getPessoa();
    }

    public salvar(): void {
        this.listagemService.salvar(this.pessoa);
        this.pessoa = new Pessoa();
    }

    public editar(pessoa: Pessoa): void {
        this.pessoa = pessoa;
    }

    public excluir(pessoa: Pessoa): void {
        this.pessoa = pessoa;
        this.listagemService.excluir(pessoa);
        this.pessoa = new Pessoa();
    }
}