import { Injectable } from '@angular/core';
import { Http }       from '@angular/http';
import { Pessoa }    from "../../model/pessoa";

@Injectable()
export class ListagemService {
    private pessoas: Array<Pessoa>;
    private countId: number;

    public constructor(private http: Http) {
        this.pessoas = [new Pessoa(1,'patrick')];
        this.countId = 0;
    }

    public getPessoa(): Array<Pessoa> {
        return this.pessoas;
    }

    public salvar(pessoa: Pessoa): void {
        if (pessoa.id == null) { // Novo usuário
            pessoa.id = ++this.countId;
            this.pessoas.push(pessoa);
        } else { // Atualização
            for (let i in this.pessoas) {
                if (this.pessoas[i].id == pessoa.id) {
                    this.pessoas[i] = pessoa;
                    break;
                }
            }
        }
    }

    public excluir(pessoa: Pessoa): void {
        for (let i in this.pessoas) {
            if (this.pessoas[i].id == pessoa.id) {
                this.pessoas.splice(parseInt(i), 1);
                break;
            }
        }
    }
}