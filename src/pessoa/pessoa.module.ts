import { NgModule }       from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';
import { HttpModule }     from "@angular/http";
import { PessoaComponent } from "./pessoa.component";
import { pessoaRouting } from "./pessoa.routing";
import {ListagemComponent} from "./listagem/listagem.component";

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        pessoaRouting
    ],
    declarations: [
        ListagemComponent
    ],
    exports: [
        //
    ]
})
export class PessoaModule {}