import { ModuleWithProviders }             from '@angular/core';
import { Routes, RouterModule }            from '@angular/router';
import { ListagemComponent } from "./listagem/listagem.component";


const pessoaRoutes: Routes = [
    {
        path: 'pessoa',
        component: ListagemComponent,

    }
];

export const pessoaRouting: ModuleWithProviders = RouterModule.forChild(pessoaRoutes);